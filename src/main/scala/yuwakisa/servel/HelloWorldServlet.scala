package yuwakisa.servel

import javax.servlet.http.{HttpServlet, HttpServletRequest, HttpServletResponse}

class HelloWorldServlet extends HttpServlet :

  override protected def doGet(request: HttpServletRequest, response: HttpServletResponse):Unit =
    Content.okText(response, "Hello world!")

package yuwakisa.servel

import org.eclipse.jetty.server.handler.ShutdownHandler
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.servlet.ServletHandler

import javax.servlet.http.{HttpServlet, HttpServletRequest, HttpServletResponse}

class ServerRunner(routes: Map[String, Class[? <: javax.servlet.Servlet]]):
  val port = 8080
  val server = new Server(8080)
  val handler = new ServletHandler()

  def start(): Unit =
    routes.foreach { case (p: String, c: Class[? <: javax.servlet.Servlet]) => handler.addServletWithMapping(c, p) }
    server.setHandler(handler)
    server.start()
    println(s"Server started on localhost:$port")
    println("Press Enter to stop the server")
    scala.io.StdIn.readLine()
    println(s"Stopping")
    server.stop()


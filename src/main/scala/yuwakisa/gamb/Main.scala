package yuwakisa.gamb

import yuwakisa.servel.{HelloWorldServlet, ResourceServlet, ServerRunner, StaticContentServlet}

object Main:
  val routes = Map( "/" -> classOf[StaticContentServlet],
    "/hello" -> classOf[HelloWorldServlet],
    "/character" -> classOf[ResourceServlet])
  val runner = new ServerRunner(routes)

  def main(args: Array[String]): Unit =
    runner.start()

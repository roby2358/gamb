package yuwakisa.gamb.model

/**
 * Represents one hand, playing all the way through to win or loss
 */
class Hand(handActions: Seq[HandAction], value: Int) :

  /**
   * Update scores and counts of the HandActions
   * @return
   */
  def updated: Seq[HandAction] =
    handActions.map { ha => ha.copy(value = value + ha.value, count = ha.count + 1) }

package yuwakisa.gamb.model

object HandAction :
  /**
   * Actions the player can take. Will worry about Double or Split later
   */
  enum Action:
    case Deal, Stand, Hit

  /**
   * The initial start state; always begin with an empty hand, which is dealt
   */
  val Empty = HandAction(0, 0, 0, Action.Deal, 0, 0)

/**
 * Given the cards a player has and the choice action they've played,
 * we track the expected value
 * @param total Total of player's cards
 * @param ace True if the player has an Ace that can be 1 or 11
 * @param show What the dealer shows
 * @param action The action the user takes
 * @param value Accumulated value from this hand & action
 * @param count The number of times we've seen this combo
 */
case class HandAction( total: Int,
                  aces: Int,
                  show: Int,
                  action: HandAction.Action,
                  value: Int,
                  count: Int)


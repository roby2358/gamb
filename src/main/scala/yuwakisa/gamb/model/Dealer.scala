package yuwakisa.gamb.model

import scala.util.Random

object Dealer :
  val R = new Random
  val ranks = Seq(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10)

/**
 * Generates hands
 */
class Dealer :
  import Dealer._

  /**
   * Deals a single card from an infinite shoe
   * @return the value and true if there's an ace
   */
  def dealOne: (Int, Int) =
    val rank = ranks(R.nextInt(ranks.length))
    (rank, if rank == 1 then 1 else 0)

  def deal: Hand =
    val start = HandAction.Empty

    // two cards to the player, one to the dealer

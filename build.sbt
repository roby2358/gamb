ThisBuild / scalaVersion := "3.1.1"

lazy val root = project
  .in(file("."))
  .settings(
    name := "yuwakisa/gamb",
    organization := "org.yuwakisa",
    version := "1.0-SNAPSHOT",

//    useScala3doc := true,

    scalacOptions ++= Seq(
      "-language:implicitConversions",
//      "-old-syntax",
//      "-new-syntax",
      "-indent",
      "-Yindent-colons"
      //      "-rewrite"
    ),

    run / run / mainClass := Some("yuwakisa.gamb.Main"),

    resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",

    libraryDependencies ++= Seq(
      "org.eclipse.jetty" % "jetty-servlet" % "9.3.12.v20160915",
      "org.eclipse.jetty" % "jetty-server" % "9.3.12.v20160915",
      "commons-io" % "commons-io" % "2.11.0",
      "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.13.1",

    ),

//    libraryDependencies := libraryDependencies.value
//      .map(_.withDottyCompat(scalaVersion.value))
)
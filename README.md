
# Running

From the command line
    sbt run

open
    http://localhost:9000

# H2

H2 website

http://www.h2database.com/html/features.html

Notes

You can browse the contents of your database by typing h2-browser at the
sbt shell
